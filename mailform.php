<?php
  require_once "recaptchalib.php";

    // Register API keys at https://www.google.com/recaptcha/admin
    $siteKey = "6LcLQwATAAAAAFbTNhpLMRUeYbCiE9TNc3i8W2aq";
    $secret = "6LcLQwATAAAAAKFNi2mAemiphbEEyAYHvRXTThmv";
    // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
    $lang = "en";
    // The response from reCAPTCHA
    $resp = null;
    // The error code from reCAPTCHA, if any
    $error = null;
    $reCaptcha = new ReCaptcha($secret);

    // Was there a reCAPTCHA response?
    if ($_POST["g-recaptcha-response"]) {
        $resp = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
    }

    if ($resp != null && $resp->success) {
        $formval=$_POST['formval'];
        $msg="";

            $headers = 'From: '.$formval['name'].' <'.$formval['email'].'> '. "\r\n".
            'X-Mailer: PHP/' . phpversion();

        foreach($formval as $index => $value)
        {
            $msg .= ucfirst($index) . ": " . $value . "\n";
        }

        if (mail("cory@torontosound.ca, aaron@torontosound.ca, quewin@torontosound.ca, adam@torontosound.ca", "Toronto Sound - General Enquiry", $msg, $headers))
                echo json_encode(array("status"=>"suc", "msg"=>"Thank you for your email. We'll be in touch shortly!"));
        else
            echo json_encode(array("status"=>"err", "msg"=>"Oops... Something went wrong. Let's try that again..."));

        die();

    }
    echo json_encode(array("status"=>"err", "msg"=>"Apparently you're not human. Try that captcha once again.", data=>$resp));

