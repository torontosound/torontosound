<?php
    define('ROOT', __DIR__ .'/../');
    
    require(ROOT."cms/projectManager.php");

    $manager = new ProjectManager();
    
    $projects = $manager->getProjectsAdmin();
    $tags = $manager->getTagsAdmin();

    if (getPost("editProject") || getPost("addProject"))
    {
        $manager->makeProject();
        exit();
    }
    elseif (getPost("delete"))
    {
        $manager->deleteProject();
        exit();
    }	
    elseif (getPost("addTag"))
    {
        $manager->addTag();
        exit();
    }	    
    elseif (getPost("deleteTags"))
    {
        $manager->deleteTags();
        exit();
    }	    

?>

<!DOCTYPE html>
<html>
<head>
    <link href="./css/cms.css" rel="stylesheet" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> 
    <script type="text/javascript" src="js/cms.js"></script>
</head>    
    
    <body>
    <section id="projectsList">
		<h2>Add New Projet</h2>
		<div class="project">
            <form id="newProject">
                <div class="poster"><img><input type="file" name="poster" required></div>
                <div class="name"><input name="name" placeholder="project name" required></div>
                <div class="url"><input name="url" placeholder="project url" required></div>
                <div class="date"><input name="addProject" type="submit"></div>
            </form>
        </div>    
		<h2>Tags</h2>
		<div class="project">
			<form id="addTag">
				<input id="tagName" name="tagName" placeholder="Tag Name">
				<input type="submit" name="addTag" value="Add">
			</form>
			<form id="removeTags">
				<div class="tags">
	                <?= $tags;?>
                </div>
				<input type="submit" name="deleteTags" value="Delete">
			</form>			
        </div>            
		<h2>Edit Projects</h2>		
        <?php
            foreach ($projects as $project)
                echo $project;
        ?>
    </section>
    </body>
</html>
    
