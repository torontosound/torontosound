$(function(){

    $(".poster input[type=file]").change(function() {
        readURL(this);
    });
    
    $("form").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            url:"./", 
            data: formData,
            processData: false,
            contentType: false,
            type:"POST",
            dataType:"json",
            success:function(data){
                alert(data.msg);
                if (data.status == "suc")
                    window.location.reload();
            }
        })
    })
    
})


function readURL(input) {
   if (input.files && input.files[0]) {
       var reader = new FileReader();
       reader.onload = function(e) {
           $(input).siblings("img").attr('src', e.target.result);
       }

       reader.readAsDataURL(input.files[0]);
   }
}
