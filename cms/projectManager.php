<?php
require (ROOT.'mustache/src/Mustache/Autoloader.php');
		//mysqli_report(MYSQLI_REPORT_ERROR);
//require ('.env.php');
class ProjectManager {

	public $list = array(); //active projects list
	
    private $mustache;

	private $projectTemplate;	
	private $tagTemplate;

	public function __construct(){
    		$this->mysqli = new mysqli("104.131.36.229", "torontosound", "aaroncoryquewinadam", "torontosound");
		if ($this->mysqli->connect_error) {
			die('Connect Error: ' . $this->mysqli->connect_error);
		}

		Mustache_Autoloader::register();
		$this->mustache = new Mustache_Engine;
		
		$this->projectTemplate = <<<EOT
		<div class="project" id="proj{{projectId}}">
			<form>
				<div class="poster"><img src="../images/uploads/{{poster}}"><input type="file" name="poster"></div>
				<div class="name"><input value="{{name}}" name="name" placeholder="name"></div>
				<div class="url"><input value="{{url}}" name="url" placeholder="url"></div>
				<div class="tags">
					{{&tags}}
				</div>
				<div class="weight">
					Weight:<br>
					<input class="weight" value="{{weight}}" name="weight">
				</div>
				<div class="date">{{date}}</div>
				<input type="hidden" value="{{projectId}}" name="id">
				<div class="editProject"><input type="submit" name="editProject" value="Edit"></div>
				<div class="editProject"><input type="submit" name="delete" value="Delete"></div>
			</form>
        </div>
EOT;

		$this->tagTemplate = <<<EOT
					<label for={{tag}}>{{tag}}</label><input name="{{id}}" id="{{tag}}" type="checkbox"{{checked}}>
EOT;
	}


/*
	Admin Functions
*/
	public function getProjectsAdmin(){
		$projects = prepare_query($this->mysqli, "SELECT * FROM projects");
		$tags = $this->getTags();


		foreach ($projects as $project)    
		{
			$tagList = array();
			$tagIds = array();
			foreach ($tags as $tag)
			{
				if (isset($tagIds[$tag['id']]) && $tag['checked'] === $project['id'] )
				{
					array_pop($tagList);
					unset($tagIds[$tag['id']]);
				}

				if ( empty($tag['checked']) || empty($tagIds[$tag['id']])  )
				{
					array_push($tagList, $this->mustache->render($this->tagTemplate, array(
						'tag' => $tag['tag'],
						'id' => $tag['id'],
						'checked' => $tag['checked'] == $project['id']?"checked":""
					)) );  


					$tagIds[$tag['id']] = true;

				}
			}
			
			$tagList = implode("", $tagList);
			array_push($this->list, $this->mustache->render($this->projectTemplate, array(
				'projectId' => $project['id'],
				'name' => $project['name'],
				'poster' => $project['poster'],
				'url' => $project['url'],
				'tags' => $tagList,
				'weight' => $project['weight'],
				'date' => $project['created']
			)));  
		}		
		return $this->list;
	}

	public function getTagsAdmin(){
		//print_r($this);
		$tags = $this->getTags();

		$tagList = array();

		foreach ($tags as $tag)    
			array_push($tagList, $this->mustache->render($this->tagTemplate, array(
				'tag' => $tag['tag'],
				'id' => $tag['id'],
				'checked' => ""
			)) ); 
		$tagList = array_unique($tagList);
		$tagList = implode("", $tagList);

		return $tagList;
	}

	public function makeProject(){

/**/
		$fileName = false;
		
		if (getFile())	///handle file uploads
		{
			$Image = getFile();
			$Image = array_shift($Image);

			if ($Image['error'] == 4)
			{
//				$return = array("status"=>"err", "err"=>"", "msg"=>"file upload error - $fileName - ".$Image['error'], "data"=>null);
//				echo json_encode($return);
//				return false;                          					
			}
			elseif ($Image['type'] !== 'image/png' 
			&& $Image['type'] !== 'image/jpeg' 
			&& $Image['type'] !== 'image/gif')
			{
				$return = array("status"=>"err", "err"=>"", "msg"=>$Image['tmp_name']." is not a valid image.", "data"=>null);
				echo json_encode($return);
				return false;
			}
			else
			{
				$fileName = "iu_".time().'.'.pathinfo($Image['name'], PATHINFO_EXTENSION);
				$fileName = strtolower($fileName);
				if (!move_uploaded_file($Image['tmp_name'], "../images/uploads/$fileName"))
				{
					$return = array("status"=>"err", "err"=>"", "msg"=>"file upload error - $fileName - ".$Image['error'], "data"=>null);
					echo json_encode($return);
					return false;                          
				}
			}						
		}
		else
		{
			$return = array("status"=>"err", "err"=>"", "msg"=>"No poster image", "data"=>null);
			echo json_encode($return);
			return false;				
		}
				
		if (getPost('id'))
		{

			$props = getPost();
			$tags = array();
			foreach ($props as $key => $value)
			{
				if ($value == "on")
				{
					array_push($tags, $key);
					unset($props[$key]);
				}
			}

			$this->mysqli->autocommit(FALSE);

			//if (!empty($tags))
			//{
				$this->setTags($props['id'], $tags);
			//}

			$params = array("sssii", getPost('name'),$fileName,getPost('url'), getPost('weight'), getPost('id'));
			$sql = "UPDATE projects SET `name` = ?, ";

			if (getFile()['poster']['error'] !== 4)
				$sql .= "`poster` = ?, ";
			else
			{
				$params[0] = "ssii";
				unset($params[2]);
				$params = array_values($params);
			}
				
			$sql .= "`url` = ?, `weight` = ? WHERE id = ?";
			$success = prepare_query($this->mysqli, $sql, $params);
		}
		else
			$success = prepare_query($this->mysqli, "INSERT INTO projects (`name`, `poster`, `url`) VALUES (?, ?, ?)", array("sss", getPost('name'),$fileName,getPost('url')) );

		if ($success)
		{
			$this->mysqli->commit();

			$return = array("status"=>"suc", "err"=>"", "msg"=>"", "data"=>null);
			
			getPost('id') ? $return['msg'] = "project successfully updated" : $return['msg'] = "project successfully created";

			echo json_encode($return);
			return true;  
		}
		else
		{
			$return = array("status"=>"err", "err"=>"", "msg"=>"", "data"=>null);
			
			getPost('id') ? $return['msg'] = "Error updating project" : $return['msg'] = "Error creating project";
			
			echo json_encode($return);
			return false;					
		}
	}


	public function addTag(){
		$success = prepare_query($this->mysqli, "INSERT INTO tags (`tag`) VALUES (?)", array("s", getPost('tagName')) );

		if ($success)
			$return = array("status"=>"suc", "err"=>"", "msg"=>"Tag added successfully", "data"=>null);
		else
			$return = array("status"=>"err", "err"=>"", "msg"=>"Could not add tag", "data"=>null);	

		echo json_encode($return);
		return false;					

	}

	private function setTags($_id, $_tags){

		$sql = "DELETE FROM projectTags WHERE `projectId` = ?";
		prepare_query($this->mysqli, $sql, array('i', $_id));

		foreach ($_tags as $tag)
			if (!prepare_query($this->mysqli, "INSERT INTO projectTags (`projectId`, `tagId`) VALUES (?, ?)", array("ii", $_id, $tag) ))
				return false;

		return true;

	}

	public function deleteProject(){
		$fileName = false;

		if (getPost('id'))
		{
			$params = array("i", getPost('id'));
			$sql = "DELETE FROM projects WHERE `id` = ?";
			$success = prepare_query($this->mysqli, $sql, $params);
		}

		if ($success)
		{
			$return = array("status"=>"suc", "err"=>"", "msg"=>"", "data"=>null);
			
			$return['msg'] = "project successfully deleted";

			echo json_encode($return);
			return true;  
		}
		else
		{
			$return = array("status"=>"err", "err"=>"", "msg"=>"", "data"=>null);
			
			$return['msg'] = "Error deleting project";
			
			echo json_encode($return);
			return false;					
		}
	}


	public function deleteTags(){
		$fileName = false;

		$tags = getPost();
		unset($tags['deleteTags']);
		$params = array();
		$success = false;

		foreach ($tags as $key=>$val)
		{
			if (empty($params))
				array_push($params, "i");	
			else
				$params[0] .="i";
			array_push($params, $key);
		}

		if (!empty($params))
		{
			$sql = "DELETE FROM tags WHERE `id` = ?";
			$success = prepare_query($this->mysqli, $sql, $params);
		}

		if ($success)
		{
			$return = array("status"=>"suc", "err"=>"", "msg"=>"", "data"=>null);
			$return['msg'] = "tags successfully deleted";
		}
		else
		{
			$return = array("status"=>"err", "err"=>"", "msg"=>"", "data"=>null);
			$return['msg'] = "Error deleting tags";
			
		}
		echo json_encode($return);
		return false;		
	}	
/*
	Main site functions
*/

	public function getProjects($tags = false){

		$params = false;
		$sql = "SELECT name, poster, url FROM projects";
		if ($tags  && $tags > -1)
		{
			$sql .= " join projectTags on projectTags.tagId = ? AND projectTags.projectId = projects.id";
			$params = array("i", $tags);
		}

		$sql .= " ORDER BY weight DESC";

		$projects = prepare_query($this->mysqli, $sql, $params);
		
		return $projects;
	}
	
	public function getTags(){
		$tags = prepare_query($this->mysqli, "SELECT tags.*, projectTags.projectId AS checked FROM tags LEFT JOIN projectTags ON projectTags.tagId = tags.id");
		
		return $tags;
	}	
}






	function prepare_query($_db, $_sql, $_params = array())
	{
//die($_sql);
		
		if ($stmt = $_db->prepare($_sql)) {

			/* bind parameters for markers */
			//$_params = $this->makeValuesReferenced($_params);

			//call_user_func_array(array($stmt, 'bind_param'), $this->makeValuesReferenced(array_merge(array($stmt), $_params))); 
			
			if (!empty($_params))
				call_user_func_array(array($stmt, 'bind_param'), makeValuesReferenced($_params)); 

			//$stmt_bind = new ReflectionFunction('mysqli_stmt_bind_param');
			//$stmt_bind->invokeArgs($_params);


			/* execute query */
			if ($stmt->execute())
			{
				$result = $stmt->get_result();
				$res = array();

				/* fetch value */
				if ($result)
				{
					while ($row = $result->fetch_assoc()) {
						array_push($res, $row);
					}
					return $res;
				}
				return true;
			}
			die ("could not set error: ".mysqli_stmt_error($stmt)."\nQuery - ".$_sql."\nParams - ".print_r($_params, true));
				
			/* close statement */
			$stmt->close($GLOBALS['stmt']);
		}	
		else
			die ("could not set error: Failed to prepare statement - \nQuery - ".$_sql."\nParams - ".print_r($_params, true));			


		return false;

	}

    function makeValuesReferenced($arr){ 
        $refs = array(); 
        foreach($arr as $key => $value) 
        $refs[$key] = &$arr[$key]; 
        return $refs; 

    } 	
	
    function getPost($post = false)
    {
        if (!$post)
            return $_POST;
        elseif (!empty($_POST[$post]))
            return $_POST[$post];
        else
            return false;
    }
    
    function getGet($get = false)
    {
        if(!$get)
            return $_GET;
        elseif (!empty($_GET[$get]))
            return $_GET[$get];
        else
            return false;
    }	
	
	
	function getFile($file = false)
    {
        if(!$file)
            return $_FILES;
        elseif (!empty($_FILES[$file]))
            return $_FILES[$file];
        else
            return false;        
    }
