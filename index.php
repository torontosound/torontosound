<?php
    define('ROOT', __DIR__ .'/');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1">
	<!--<script src="scripts/prefixfree.min.js"></script> -->

	<meta property="og:title" content="Toronto Sound"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="http://torontosound.ca/"/>
	<meta property="og:image" content="http://torontosound.ca/img/toronto-sound-icon.png"/>
	<meta property="og:site_name" content="Location Sound &bull; Post-Production &bull; Music Composition | Toronto's Premier Audio Agency"/>
	<meta property="fb:admins" content="510034041"/>
	<meta property="fb:admins" content="501690139"/>


	<title>Toronto Sound // Location + Post + Composition</title>
	<link href="./css/main.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="./scripts/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

	<script src="scripts/touche.min.js"></script> 
	<script src="scripts/jquery.min.js"></script> 
	<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>	
	<script type="text/javascript" src="./scripts/fancybox/jquery.fancybox-1.3.4.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jcarousel/0.3.4/jquery.jcarousel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jcarousel/0.3.4/jquery.jcarousel-pagination.min.js"></script>
	<script src="scripts/mustache.js"></script> 
	<script src='https://www.google.com/recaptcha/api.js'></script>


	<script id="projectTemplate" type="x-tmpl-mustache">
	
		<svg href="{{url}}" rel="prettyPhoto" version="1.1" id="{{hexId}}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="projectHexigon {{transClass}} {{rowClass}}" viewBox="0 0 54 62" enable-background="new 0 0 54 62" xml:space="preserve" preserveAspectRatio="xMinYMax meet">
			<defs>
				<pattern id="{{patternId}}" x="0" y="0" patternUnits="userSpaceOnUse" height="100%" width="100%">
					<image height="100%" width="100%" xlink:href="{{imageUrl}}"></image>
				</pattern>
			</defs>
			<polygon fill="url(#{{patternId}}) white" points="54,46.5 27,62 0,46.5 0,15.5 27,0 54,15.5 "/>
		</svg>
	
</script>

<script id="filterTemplate" type="x-tmpl-mustache">
		<li><a href="#{{id}}">{{tag}}</a></li>
</script>


</head>

<body>
<header id="header">
	<div id="logo-bar">
		<a href="" id="logo">
			<div class="logo-text">TORONTO</div>
			<div class="logo-text" id="logo-text-2">SOUND</div>
			<div id="logo-cube"></div>
			
		</a>
	</div>
	<div class="menu-btn"></div>
</header>
<div id="logo-triangle"></div>
<nav class="nav">
	<?php require "nav.php"; ?>
</nav>

<div id="skrollr-body" class="perspective">
<section class="content-container" id="promo-video"> 
	<div id="videoOverlay"></div>
	<video id="introVid" src="videos/ts-intro.mp4" poster="./images/poster.png" onclick="this.play()" controls/></video>
	<!--<a href="#what-we-do" class="nextSection"></a>-->

</section>
<section class="content-container" id="what-we-do">
<div class="section-split">
	<div class="what-we-do-title">ON SET</div>
	<div class="what-we-do-title sub-heading">where the experience begins</div>
</div>
<div class="section-split">
	<div class="what-we-do-title">IN STUDIO</div>
	<div class="what-we-do-title sub-heading">where the vision is realized</div>
</div>
</section>
<section class="content-container" id="space">
	<!--<div id="revery-music-group-img"></div>-->
	<div class="hexagon-separator" id="space-hex-position">
		<div class="hexagon-separator-content" id="hexagon-separator-space">our space</div>
	</div>	
	<div class="space-text">
	</div>	
	<div class="jcarousel-wrapper">
		<div class="jcarousel">
			<ul>
				<li style="background: url('./images/studio_04.jpg');"></li>
				<li style="background: url('./images/studio_05.jpg');"></li>
				<li style="background: url('./images/studio_03.jpg');"></li>
				<li style="background: url('./images/studio_01.jpg');"></li>
			</ul>
		</div>
		<p class="jcarousel-pagination" data-jcarouselpagination="true"></p>


		<!--<a href="#1" class="active">1</a><a href="#2">2</a><a href="#3">3</a><a href="#4">4</a><a href="#5">5</a>-->
	</div>
	<div class="hexagon-separator" id="services-hex-position">
		<div class="hexagon-separator-content" id="hexagon-separator-services">services</div>
	</div>
</section>

<section class="content-container transformZero" id="services">
	<div class="service-bg parallaxLayer parallax-3d"></div>
	<div id="services-container">
		<div class="service-box ">
			<div class="service-icon" id="service-location-audio"></div>
			<div class="service-title">Location Audio</div>
			<div class="service-description">
				By starting with a great recording on set, you create a foundation to build an exceptional end product. Our experienced team and state-of-the-art equipment provide you with the highest quality on-set recording possible.
			</div>
		</div>
		<div class="service-box ">
			<div class="service-icon" id="service-mixing"></div>
			<div class="service-title">Mixing & Mastering</div>
			<div class="service-description">
				Professional mixing & mastering in our acoustically treated control rooms allows for the highest quality of sound. Our engineers use a combination of editing, noise reduction and advanced mixing techniques to emphasize the tone of your piece.
			</div>
		</div>
		<div class="service-box ">
			<div class="service-icon" id="service-mastering"></div>
			<div class="service-title">Voiceover & ADR</div>
			<div class="service-description">
				Whether commercial voiceovers or re-recording on screen talent, capture dialogue in our pristine and controlled environment.
			</div>
		</div>
		<br>
		<div class="service-box ">
			<div class="service-icon" id="service-sound-design"></div>
			<div class="service-title">Sound Design</div>
			<div class="service-description">
				Custom created sounds from a professional environment creatively reinforce your vision. We work with the world’s largest professional sound effects libraries and our own private library of custom sounds.
			</div>
		</div>
		<div class="service-box ">
			<div class="service-icon" id="service-music-licensing"></div>
			<div class="service-title">Music Licensing</div>
			<div class="service-description">
				We curate the process of music supervision, using our vast and diverse catalogue, as well as direct access to the major publishing companies across North America.
			</div>
		</div>
		<div class="service-box ">
			<div class="service-icon" id="service-music-composition"></div>
			<div class="service-title">Music Composition</div>
			<div class="service-description">
				Custom music created specifically by our composers & producers to reinforce your video’s natural dynamic. This highlights the tone and character of your content using the universal language of music.
			</div>
		</div>
	</div>
</section>
<section class="content-container" id="works">
	<div class="hexagon-separator">
		<div class="hexagon-separator-content" id="hexagon-separator-works"><span id="worksJump">our work</span></div>
		<div class="hexagon-separator-works-filters">filters</div>
		<div class="hexagon-separator-works-filters-menu">
			<ul id="filterMenu">
				<li><a href="#-1">All</a></li>
			</ul>
			<div class="filters-menu-triangle"></div>
		</div>
	</div>
	<div id="projectsWrapper">
		<div class="projectsArrow" id="leftArrow"></div>
		<div id="projectsWrapperInner"></div>
		<div id="projectsWrapperShadow"></div>
		<div class="projectsArrow" id="rightArrow"></div>
	</div>
</section>
<section class="content-container" id="team">
	<div class="team-left">
		<h1 class="team-title">our team</h1>
		<div class="team-menu">
			<ul>
				<li id="team-menu-adam"><a href="#team-adam">Adam</a></li>
				<li id="team-menu-aaron"><a href="#team-aaron">Aaron</a></li>
				<li id="team-menu-cory"><a href="#team-cory">Cory</a></li>
				<li id="team-menu-quewin"><a href="#team-quewin">Quewin</a></li>
			</ul>
		</div>
		<section class="team-description" id="team-adam">
			<h2 class="team-position">Leader and perfectionist</h2>
			<p class="team-description-text">
				Having worked on thousands of location sound jobs in the Toronto film industry, Adam brings a wealth of knowledge and experience to every sound project. Adam's attention to detail and strong leadership skills has ensured great success for clients small and large.
			</p>
			<div class="team-picture" id="team-picture-adam"></div>
		</section>

		<section class="team-description" id="team-aaron">
			<h2 class="team-position">Artist, designer and entrepreneur</h2>
			<p class="team-description-text">
				Aaron has extensive experience in audio production and engineering, including a strong background in post production. With music as Aaron’s main point of passion, he has produced and composed for a wide range of projects, bringing a unique and refined approach to Toronto Sound’s compositions.
			</p>
			<div class="team-picture" id="team-picture-aaron"></div>
		</section>
		
		<section class="team-description" id="team-cory">
			<h2 class="team-position">Innovator and producer</h2>
			<p class="team-description-text">
				Cory comes from a history in the music business. Having worked with and studied under some of the country’s best producers and engineers, giving him exceptional understanding of music and composition. More than anything, innovating and collaborating with like-minded creatives is where he shines.
			</p>
			<div class="team-picture" id="team-picture-cory"></div>
		</section>
		
		<section class="team-description" id="team-quewin">
			<h2 class="team-position">Creative and artisan</h2>
			<p class="team-description-text">
				Quewin’s versatility and creative dexterity sets him at the forefront of our artistic endeavors. His sound design & music production experience has provided him with an acute understanding of the emotional subtleties inherent in multimedia. Obsessive about his craft, he constantly seeks to redefine his creative boundaries.
			</p>
			<div class="team-picture" id="team-picture-quewin"></div>
		</section>
	</div>
</section>
<section class="content-container" id="connect">
	<div class="hexagon-separator" id="hexagon-separator-connect-align">
		<div class="hexagon-separator-content" id="hexagon-separator-connect">connect</div>
	</div>
	<form id="connect-form">

		<div class="service-title" style="color:#2e77bc; padding:10px;"></div>
		<div id="inputs">
			<div class="input">
				<label class="label">Your Name *</label>
				<input class="text-field" type="text" name="formval[name]" value="" required="">
			</div>
			<div class="input">
				<label class="label">Your Email *</label>
				<input class="text-field" type="email" name="formval[email]" value="" required="">
			</div>
			<div class="input">
				<label class="label">Confirm Your Email *</label>
				<input class="text-field" type="email" name="emailComp" value="" required="">
			</div>
			<div class="input">
				<label class="label">Phone</label>
				<input class="text-field" type="text" name="formval[phone]" value="">
			</div>
			<div class="input">
				<label class="label">Company</label>
				<input class="text-field" type="text" name="formval[company]" value="">
			</div>
			<div class="input">
				<label class="label">Website</label>
				<input class="text-field" type="text" name="formval[website]" value="">
			</div>
			<div class="input" id="text-area">
				<label class="label">Message *</label>
				<textarea class="text-area" name="formval[message]" required=""></textarea>
			</div>
			<div id="captcha">
				<label class="label"></label>
				<div class="g-recaptcha" data-sitekey="6LcLQwATAAAAAFbTNhpLMRUeYbCiE9TNc3i8W2aq"></div>
			</div>
		</div>
		<div id="submit-box">
			<div id="error-box">
			</div>
			<input type="submit" id="submit-btn">
		</div>
	</form>
	<div id="location-info">
		<div id="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11546.503101304195!2d-79.41680544962753!3d43.65595369571266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b34eeb23dfaf1%3A0x440ae0f93b287bdc!2sToronto+Sound!5e0!3m2!1sen!2sca!4v1464239957015" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div id="address">
			298 Markham St #4<br>
			Toronto, ON<br>
			M6J 2G6<br>
			<a href="mailto:info@torontosound.ca">info@torontosound.ca</a><br>
			<a href="tel:+18007530086">1.800.753.0086</a>
		</div>
	</div>
	<footer id="footer">
		<div id="connect-social-box">
			<a href="https://www.facebook.com/TorontoSound" class="social-icon" id="facebook" target="_blank"></a>
			<a href="https://twitter.com/TorontoSound" class="social-icon" id="twitter" target="_blank"></a>
			<a href="https://plus.google.com/114023630788082314625" rel="publisher" class="social-icon" id="google" target="_blank"></a>
			<a href="https://www.linkedin.com/company/toronto-sound/" class="social-icon" id="in" target="_blank"></a>
		</div>
		<div id="copyright">&copy <?=date('Y')?> torontosound.ca</div>
		<div href="" id="tag">
			<a href="http://throwawaygames.ca" target="_blank">Powered By:</br>
			Throw Away Games</a>
		</div>
	</div>
</section>
</div>
	<div class="service-bg parallax-2d"></div>

<script src="./scripts/main.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      
        ga('create', 'UA-5069684-39', 'torontosound.ca');
        ga('send', 'pageview');
  
    </script>
</body>
