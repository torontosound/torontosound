<?php
    define('ROOT', __DIR__ .'/');

    require(ROOT."cms/projectManager.php");

    $manager = new ProjectManager();
    
    if (!getGet() || getGet('tagId'))
    {
        $projects = $manager->getProjects(getGet('tagId'));
        $return = array("status"=>"suc", "err"=>"", "msg"=>"", "data"=>$projects);
        echo json_encode($return);
        exit();
    }
    elseif(getGet('filters'))
    {
        $filters = $manager->getTags();

	$tagIds = array();
	foreach ($filters as $key=>$tag)
	{
		if ( empty($tagIds[$tag['id']])  )
		{
			$tagIds[$tag['id']] = true;
		}
		else
			unset($filters[$key]);
	}
	
        $return = array("status"=>"suc", "err"=>"", "msg"=>"", "data"=>array_values($filters));
        echo json_encode($return);
        exit();
        
    }
